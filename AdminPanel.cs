﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace kursach
{
    public partial class AdminPanel : Form
    {
        SqlConnection dataBaseConnection = Resources.getConnection();
        DataSet ds;
        int AccLevel;
        int rights;
        String login1;
        public AdminPanel(String lg, int rights)
        {
            this.rights = rights;
            login1 = lg;
            InitializeComponent();
        }

        private void AdminPanel_Load(object sender, EventArgs e)
        {
            if (rights == 6)
            {
                radioButton6.Visible = true;
            }
            RefreshData(1);
            ControlBox = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChangePass()
        {
            String pass = textBox1.Text;
            SqlCommand cmd = new SqlCommand("Update u0250751_enotik.Rights set пароль = '" + pass + "' where код = " + dataGridView1["код", dataGridView1.CurrentCell.RowIndex].Value, dataBaseConnection);
            try
            {
                dataBaseConnection.Open();
                cmd.ExecuteNonQuery();
                dataBaseConnection.Close();
                RefreshData(1);
                textBox1.Clear();
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void RefreshData(int numGr)
        {
            ds = new DataSet();
            SqlDataAdapter da;
            try
            {
                if (numGr == 1)
                {
                    da = new SqlDataAdapter("Select тип_аккаунта as 'Тип аккаунта', пароль, код from u0250751_enotik.Rights", dataBaseConnection);
                    da.Fill(ds, "Пароли");
                    dataGridView1.DataSource = ds.Tables["Пароли"];
                    dataGridView1.Columns[2].Visible = false;
                    dataGridView1.Update();
                }
                else
                {
                    da = new SqlDataAdapter("Select userName as 'login', pass as 'password', Name as 'Имя', Rights as 'rights', isBlocked as 'block' from u0250751_enotik.Users where userName like '" + textBox2.Text + "%'", dataBaseConnection);
                    da.Fill(ds, "Доступ");
                    dataGridView2.DataSource = ds.Tables["Доступ"];
                    dataGridView2.Columns[3].Visible = false;
                    dataGridView2.Columns[4].Visible = false;
                    dataGridView2.Update();
                    checkBlock();
                }
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void setCheck()
        {
            int level;
            level = (int)dataGridView2["rights", dataGridView2.CurrentCell.RowIndex].Value;
            switch (level)
            {
                case 1:
                    radioButton1.Checked = true;
                    break;
                case 2:
                    radioButton2.Checked = true;
                    break;
                case 3:
                    radioButton3.Checked = true;
                    break;
                case 4:
                    radioButton4.Checked = true;
                    break;
                case 5:
                    radioButton6.Checked = true;
                    break;
            }
        }

        private void CLoseBoxes()
        {
            groupBox2.Visible = false;
            groupBox1.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                if (MessageBox.Show("Убрать пароль для типа аккаунта?", "Подтверждение действия", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ChangePass();
                    groupBox1.Visible = false;
                    groupBox2.Visible = false;
                }
                else
                    return;
            }
            else
            {
                ChangePass();
                CLoseBoxes();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CLoseBoxes();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CLoseBoxes();
            groupBox1.Visible = true;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Подтверждаете изменение уровня доступа учетной записи?", "Подтверждение действия", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    DateTime time = DateTime.Today;
                    String login = (String)dataGridView2["login", dataGridView2.CurrentCell.RowIndex].Value;
                    SqlCommand cmd = new SqlCommand("Update u0250751_enotik.Users set Rights = " + AccLevel + " where userName = '" + login + "'", dataBaseConnection);
                    SqlCommand cmd1 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + login1 + "', 'Изменил уровень доступа у " + login + " на " + AccLevel + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                    dataBaseConnection.Open();
                    cmd.ExecuteNonQuery();
                    cmd1.ExecuteNonQuery();
                    dataBaseConnection.Close();
                    MessageBox.Show("Уровень доступа аккаунта был изменен.", "Успешное выполнение операции", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    groupBox2.Visible = false;
                }
                else
                    return;
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
                AccLevel = 1;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
                AccLevel = 2;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
                AccLevel = 3;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton4.Checked)
                AccLevel = 4;
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton6.Checked)
                AccLevel = 5;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Уверены в том, что хотите удалить учетную запись?", "Подтверждение операции", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    DateTime time = DateTime.Today;
                    String login = (String)dataGridView2["login", dataGridView2.CurrentCell.RowIndex].Value;
                    SqlCommand cmd = new SqlCommand("Delete from u0250751_enotik.Users where userName = '" + login + "'", dataBaseConnection);
                    SqlCommand cmd1 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + login1 + "', 'Удалил учетную запись " + login + "', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                    dataBaseConnection.Open();
                    cmd.ExecuteNonQuery();
                    cmd1.ExecuteNonQuery();
                    dataBaseConnection.Close();
                    MessageBox.Show("Удаление выполнено.", "Успешное выполнение операции", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    groupBox2.Visible = false;
                }
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            setCheck();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            RefreshData(2);
            setCheck();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            CLoseBoxes();
            groupBox2.Visible = true;
            RefreshData(2);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            CLoseBoxes();
            textBox2.Clear();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            DateTime time = DateTime.Today;
            String login = (String)dataGridView2["login", dataGridView2.CurrentCell.RowIndex].Value;
            SqlCommand cmd = new SqlCommand("Update u0250751_enotik.Users set isBlocked=0 where userName = '" + login + "'", dataBaseConnection);
            SqlCommand cmd1 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + login1 + "', 'Разблокировал " + login + "', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
            try
            {
                dataBaseConnection.Open();
                cmd.ExecuteNonQuery();
                cmd1.ExecuteNonQuery();
                dataBaseConnection.Close();
                MessageBox.Show("Пользователь разблокирован", "Успешно разблокирован", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ShowHideBlock("hide");
                RefreshData(2);
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void ShowHideBlock(String mode)
        {
            if (mode.Equals("show"))
            {
                label5.Visible = true;
                button9.Visible = true;
            }
            else
            {
                label5.Visible = false;
                button9.Visible = false;
            }
        }

        private void checkBlock()
        {
            bool block = (bool)dataGridView2["block", dataGridView2.CurrentCell.RowIndex].Value;
            if (block)
            {
                ShowHideBlock("show");
            }
            else
            {
                ShowHideBlock("hide");
            }
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            checkBlock();
        }

        private void dataGridView2_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            checkBlock();
        }
    }
}
