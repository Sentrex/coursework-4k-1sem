﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace kursach
{
    public partial class Employees : Form
    {
        SqlConnection dataBaseConnection = Resources.getConnection();
        DataSet ds;
        SqlDataAdapter da;
        String login;
        DateTime time = DateTime.Today;
        public Employees(String user)
        {
            login = user;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Height = 639;
            RegistrationEmployee.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DateTime time = DateTime.Today;
            try
            {
                if (MessageBox.Show("Вы подтвержадаете выполнение действия? Его выполнение послечет за собой удаление всех упоминаний данного сотрудника в базе", "Подтверждение увольнения", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    int selectedCode = (int)dataGridView1["id", dataGridView1.CurrentCell.RowIndex].Value;
                    if (((String)dataGridView1["Должность", dataGridView1.CurrentCell.RowIndex].Value).Equals("директор"))
                    {
                        MessageBox.Show("Попытка удаления директора из базы. Запись о попытке внесена в лог! Ваш аккаунт будет заблокирован.", "Попытка увольнения отклонена", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        SqlCommand cmd2 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + login + "', 'Попытка удаления директора! Аккаунт заблокирован', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                        SqlCommand cmd3 = new SqlCommand("Update u0250751_enotik.Users set isBlocked=1 where userName = '" + login + "'", dataBaseConnection);
                        dataBaseConnection.Open();
                        cmd2.ExecuteNonQuery();
                        cmd3.ExecuteNonQuery();
                        dataBaseConnection.Close();
                        Resources.getAuth().Visible = true;
                        Resources.getAuth().Enabled = true;
                        Resources.getMainForm().Close();
                        this.Close();
                        return;
                    }
                    String query = "";
                    query += "Delete from u0250751_enotik.Nazn_Projects where id_sotr = " + selectedCode + ";";
                    query += "Delete from u0250751_enotik.Employee where id = " + selectedCode;
                    SqlCommand cmd = new SqlCommand(query, dataBaseConnection);
                    SqlCommand cmd1 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + login + "', 'Уволил сотрудника " + (String)dataGridView1["ФИО", dataGridView1.CurrentCell.RowIndex].Value + "', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                    dataBaseConnection.Open();
                    cmd.ExecuteNonQuery();
                    cmd1.ExecuteNonQuery();
                    dataBaseConnection.Close();
                    MessageBox.Show("Удаление выполнено. Все упоминания об этом сотруднике также были удалены.", "Успешное выполнение команды", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    RefreshData();
                }
                else
                    return;
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RefreshData()      //обновление данных на форме
        {
            ds = new DataSet();
            try
            {
                String search = textBox1.Text;
                da = new SqlDataAdapter("Select name as ФИО, (select nazv from u0250751_enotik.Dolzn where id = id_dolzn) as Должность, id from u0250751_enotik.Employee where name like '" + search + "%' order by name", dataBaseConnection);
                da.Fill(ds, "Штат");
                dataGridView1.DataSource = ds.Tables["Штат"];
                dataGridView1.Columns[2].Visible = false;
                dataGridView1.Update();
                SqlDataAdapter da1;
                da1 = new SqlDataAdapter("select * from u0250751_enotik.Dolzn", dataBaseConnection);
                DataTable tbl = new DataTable();
                da1.Fill(tbl);
                Dolzn.DataSource = tbl;
                Dolzn.DisplayMember = "nazv";
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void Employees_Load(object sender, EventArgs e)
        {
            RefreshData();
            this.ControlBox = false;
            this.Height = 460;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            String fio = FIO.Text;      //получение фио клиента
            String dolzn = Dolzn.Text;  //получение должности
            if (FIO.Text.Equals(""))
            {
                MessageBox.Show("Введите ФИО нового сотрудника", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (Dolzn.Text.Equals(""))
            {
                MessageBox.Show("Выберите должность нового сотрудника", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                if (Dolzn.Text.Equals("директор"))
                {
                    MessageBox.Show("Добавление директора в базу запрещено.", "Попытка регитрации отклонена", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    SqlCommand cmd3 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + login + "', 'Попытка регистрации пользователя как директора! ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                    dataBaseConnection.Open();
                    cmd3.ExecuteNonQuery();
                    dataBaseConnection.Close();
                    return;
                }
                SqlCommand cmd = new SqlCommand("Insert into u0250751_enotik.Employee values('" + fio + "', (select id from u0250751_enotik.Dolzn where nazv = '" + dolzn + "'))");
                cmd.Connection = dataBaseConnection;
                dataBaseConnection.Open();
                cmd.ExecuteNonQuery();
                dataBaseConnection.Close();
                SqlCommand cmd1 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + login + "', 'Принял на работу " + fio + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                dataBaseConnection.Open();
                cmd1.ExecuteNonQuery();
                dataBaseConnection.Close();
                RegistrationEmployee.Visible = false;
                FIO.Clear();
                RefreshData();
                this.Height = 460;
            }
            catch (SqlException)
            {
                MessageBox.Show("Ошибка регистрации сотрудника. Проверьте вводимые данные.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SqlCommand cmd1 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + login + "', 'Ошибка регистрации нового сотрудника " + fio + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                cmd1.ExecuteNonQuery();
                dataBaseConnection.Close();
                Dolzn.Text = "";
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Height = 460;
            RegistrationEmployee.Visible = false;
            FIO.Clear();
        }
    }
}
