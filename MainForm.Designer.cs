﻿namespace kursach
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.пользовательToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сменитьПользователяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходИзПрограммыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отделыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отделКадровToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.контрольПерсоналаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.контрольДолжностейToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отделУправленияПроектамиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.управлениеНовымиПроектамиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.управлениеСущствующимиПроектамиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.архивЗавершенныхПроектовToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отделРаботыСКлиентамиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.логИзмененийБазыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.графикиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ожидаемыеПоступленияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.статистикаПоПроектамToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.admPanel = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("a_CooperBlackRg", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(344, 411);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 19);
            this.label4.TabIndex = 31;
            this.label4.Text = "label4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("a_ConceptoTitulNrFy", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(62, 413);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(276, 18);
            this.label3.TabIndex = 30;
            this.label3.Text = "Тип аккаунта пользователя:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("AdverGothic Ho", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.Coral;
            this.label2.Location = new System.Drawing.Point(351, 223);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 19);
            this.label2.TabIndex = 29;
            this.label2.Text = "IT company";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("a_AvanteDrp", 50.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Green;
            this.label1.Location = new System.Drawing.Point(289, 154);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(227, 78);
            this.label1.TabIndex = 27;
            this.label1.Text = "BLAZZ";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.пользовательToolStripMenuItem,
            this.отделыToolStripMenuItem,
            this.отчетыToolStripMenuItem,
            this.оПрограммеToolStripMenuItem,
            this.admPanel,
            this.выходToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(789, 24);
            this.menuStrip1.TabIndex = 32;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // пользовательToolStripMenuItem
            // 
            this.пользовательToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.сменитьПользователяToolStripMenuItem,
            this.выходИзПрограммыToolStripMenuItem});
            this.пользовательToolStripMenuItem.Name = "пользовательToolStripMenuItem";
            this.пользовательToolStripMenuItem.Size = new System.Drawing.Size(96, 20);
            this.пользовательToolStripMenuItem.Text = "Пользователь";
            // 
            // сменитьПользователяToolStripMenuItem
            // 
            this.сменитьПользователяToolStripMenuItem.Name = "сменитьПользователяToolStripMenuItem";
            this.сменитьПользователяToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.сменитьПользователяToolStripMenuItem.Text = "Сменить пользователя";
            this.сменитьПользователяToolStripMenuItem.Click += new System.EventHandler(this.сменитьПользователяToolStripMenuItem_Click);
            // 
            // выходИзПрограммыToolStripMenuItem
            // 
            this.выходИзПрограммыToolStripMenuItem.Name = "выходИзПрограммыToolStripMenuItem";
            this.выходИзПрограммыToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.выходИзПрограммыToolStripMenuItem.Text = "Выход из программы";
            this.выходИзПрограммыToolStripMenuItem.Click += new System.EventHandler(this.выходИзПрограммыToolStripMenuItem_Click);
            // 
            // отделыToolStripMenuItem
            // 
            this.отделыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.отделКадровToolStripMenuItem,
            this.отделУправленияПроектамиToolStripMenuItem,
            this.отделРаботыСКлиентамиToolStripMenuItem});
            this.отделыToolStripMenuItem.Name = "отделыToolStripMenuItem";
            this.отделыToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.отделыToolStripMenuItem.Text = "Отделы";
            // 
            // отделКадровToolStripMenuItem
            // 
            this.отделКадровToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.контрольПерсоналаToolStripMenuItem,
            this.контрольДолжностейToolStripMenuItem});
            this.отделКадровToolStripMenuItem.Name = "отделКадровToolStripMenuItem";
            this.отделКадровToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.отделКадровToolStripMenuItem.Text = "Отдел кадров";
            // 
            // контрольПерсоналаToolStripMenuItem
            // 
            this.контрольПерсоналаToolStripMenuItem.Name = "контрольПерсоналаToolStripMenuItem";
            this.контрольПерсоналаToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.контрольПерсоналаToolStripMenuItem.Text = "Контроль персонала";
            this.контрольПерсоналаToolStripMenuItem.Click += new System.EventHandler(this.контрольПерсоналаToolStripMenuItem_Click);
            // 
            // контрольДолжностейToolStripMenuItem
            // 
            this.контрольДолжностейToolStripMenuItem.Name = "контрольДолжностейToolStripMenuItem";
            this.контрольДолжностейToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.контрольДолжностейToolStripMenuItem.Text = "Контроль должностей";
            this.контрольДолжностейToolStripMenuItem.Click += new System.EventHandler(this.контрольДолжностейToolStripMenuItem_Click);
            // 
            // отделУправленияПроектамиToolStripMenuItem
            // 
            this.отделУправленияПроектамиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.управлениеНовымиПроектамиToolStripMenuItem,
            this.управлениеСущствующимиПроектамиToolStripMenuItem,
            this.архивЗавершенныхПроектовToolStripMenuItem});
            this.отделУправленияПроектамиToolStripMenuItem.Name = "отделУправленияПроектамиToolStripMenuItem";
            this.отделУправленияПроектамиToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.отделУправленияПроектамиToolStripMenuItem.Text = "Отдел управления проектами";
            // 
            // управлениеНовымиПроектамиToolStripMenuItem
            // 
            this.управлениеНовымиПроектамиToolStripMenuItem.Name = "управлениеНовымиПроектамиToolStripMenuItem";
            this.управлениеНовымиПроектамиToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.управлениеНовымиПроектамиToolStripMenuItem.Text = "Управление новыми проектами";
            this.управлениеНовымиПроектамиToolStripMenuItem.Click += new System.EventHandler(this.управлениеНовымиПроектамиToolStripMenuItem_Click);
            // 
            // управлениеСущствующимиПроектамиToolStripMenuItem
            // 
            this.управлениеСущствующимиПроектамиToolStripMenuItem.Name = "управлениеСущствующимиПроектамиToolStripMenuItem";
            this.управлениеСущствующимиПроектамиToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.управлениеСущствующимиПроектамиToolStripMenuItem.Text = "Назначить проект";
            this.управлениеСущствующимиПроектамиToolStripMenuItem.Click += new System.EventHandler(this.управлениеСущствующимиПроектамиToolStripMenuItem_Click);
            // 
            // архивЗавершенныхПроектовToolStripMenuItem
            // 
            this.архивЗавершенныхПроектовToolStripMenuItem.Name = "архивЗавершенныхПроектовToolStripMenuItem";
            this.архивЗавершенныхПроектовToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.архивЗавершенныхПроектовToolStripMenuItem.Text = "Завершить проект";
            this.архивЗавершенныхПроектовToolStripMenuItem.Click += new System.EventHandler(this.архивЗавершенныхПроектовToolStripMenuItem_Click);
            // 
            // отделРаботыСКлиентамиToolStripMenuItem
            // 
            this.отделРаботыСКлиентамиToolStripMenuItem.Name = "отделРаботыСКлиентамиToolStripMenuItem";
            this.отделРаботыСКлиентамиToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.отделРаботыСКлиентамиToolStripMenuItem.Text = "Отдел работы с клиентами";
            this.отделРаботыСКлиентамиToolStripMenuItem.Click += new System.EventHandler(this.отделРаботыСКлиентамиToolStripMenuItem_Click);
            // 
            // отчетыToolStripMenuItem
            // 
            this.отчетыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.логИзмененийБазыToolStripMenuItem,
            this.графикиToolStripMenuItem,
            this.ожидаемыеПоступленияToolStripMenuItem,
            this.статистикаПоПроектамToolStripMenuItem});
            this.отчетыToolStripMenuItem.Name = "отчетыToolStripMenuItem";
            this.отчетыToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.отчетыToolStripMenuItem.Text = "Отчеты";
            // 
            // логИзмененийБазыToolStripMenuItem
            // 
            this.логИзмененийБазыToolStripMenuItem.Name = "логИзмененийБазыToolStripMenuItem";
            this.логИзмененийБазыToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.логИзмененийБазыToolStripMenuItem.Text = "Лог изменений базы";
            this.логИзмененийБазыToolStripMenuItem.Click += new System.EventHandler(this.логИзмененийБазыToolStripMenuItem_Click);
            // 
            // графикиToolStripMenuItem
            // 
            this.графикиToolStripMenuItem.Name = "графикиToolStripMenuItem";
            this.графикиToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.графикиToolStripMenuItem.Text = "Графики";
            this.графикиToolStripMenuItem.Click += new System.EventHandler(this.графикиToolStripMenuItem_Click);
            // 
            // ожидаемыеПоступленияToolStripMenuItem
            // 
            this.ожидаемыеПоступленияToolStripMenuItem.Name = "ожидаемыеПоступленияToolStripMenuItem";
            this.ожидаемыеПоступленияToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.ожидаемыеПоступленияToolStripMenuItem.Text = "Ожидаемые поступления";
            this.ожидаемыеПоступленияToolStripMenuItem.Click += new System.EventHandler(this.ожидаемыеПоступленияToolStripMenuItem_Click);
            // 
            // статистикаПоПроектамToolStripMenuItem
            // 
            this.статистикаПоПроектамToolStripMenuItem.Name = "статистикаПоПроектамToolStripMenuItem";
            this.статистикаПоПроектамToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.статистикаПоПроектамToolStripMenuItem.Text = "Статистика по проектам";
            this.статистикаПоПроектамToolStripMenuItem.Click += new System.EventHandler(this.статистикаПоПроектамToolStripMenuItem_Click);
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.оПрограммеToolStripMenuItem.Text = "О программе";
            this.оПрограммеToolStripMenuItem.Click += new System.EventHandler(this.оПрограммеToolStripMenuItem_Click);
            // 
            // admPanel
            // 
            this.admPanel.Name = "admPanel";
            this.admPanel.Size = new System.Drawing.Size(154, 20);
            this.admPanel.Text = "Панель администратора";
            this.admPanel.Visible = false;
            this.admPanel.Click += new System.EventHandler(this.admPanel_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("a_ConceptoTitulNrFy", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(62, 382);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(191, 18);
            this.label5.TabIndex = 33;
            this.label5.Text = "Имя пользователя: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("a_CooperBlackRg", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label6.Location = new System.Drawing.Point(250, 380);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 19);
            this.label6.TabIndex = 34;
            this.label6.Text = "label6";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(789, 440);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Компания BLAZZ";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem отделыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отделКадровToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem контрольПерсоналаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem контрольДолжностейToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отделУправленияПроектамиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отделРаботыСКлиентамиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчетыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem логИзмененийБазыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem admPanel;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolStripMenuItem пользовательToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сменитьПользователяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходИзПрограммыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem графикиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ожидаемыеПоступленияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem управлениеНовымиПроектамиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem управлениеСущствующимиПроектамиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem архивЗавершенныхПроектовToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem статистикаПоПроектамToolStripMenuItem;
    }
}