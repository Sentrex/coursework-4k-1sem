﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace kursach
{
    public partial class Moneys : Form
    {
        SqlConnection dataBaseConnection = Resources.getConnection();
        DataSet ds;
        SqlDataAdapter da;
        DateTime time = DateTime.Today;
        public Moneys()
        {
            InitializeComponent();
        }

        private void Moneys_Load(object sender, EventArgs e)
        {
            ds = new DataSet();
            try
            {
                da = new SqlDataAdapter("select sum(summa) as 'Поступления', count(*) as col from u0250751_enotik.Projects where id in (select id_project from u0250751_enotik.Nazn_Projects where complete = 0) ", dataBaseConnection);
                da.Fill(ds, "Отчет");
                Double sum = Double.Parse(ds.Tables["Отчет"].Rows[0][0].ToString());
                String col = ds.Tables["Отчет"].Rows[0][1].ToString();
                label4.Text = sum.ToString();
                label2.Text = col;
                this.ControlBox = false;
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
