﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace kursach
{
    public partial class Projects : Form
    {
        SqlConnection dataBaseConnection = Resources.getConnection();
        DataSet ds;
        SqlDataAdapter da;
        String login;
        DateTime time = DateTime.Today;
        public Projects(String user)
        {
            login = user;
            InitializeComponent();
        }

        private void Projects_Load(object sender, EventArgs e)
        {
            RefreshData();
            showSum();
            this.Width = 505;
            this.ControlBox = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataSet ds1 = new DataSet();
            this.Width = 965;
            regProject.Visible = true;
            try
            {
                showClients();
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FIO_TextChanged(object sender, EventArgs e)
        {
            label13.Text = name.Text;
            if (name.Text.Equals(""))
            {
                label13.Visible = false;
            }
            else
            {
                label13.Visible = true;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Width = 505;
            regProject.Visible = false;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            String text = textBox3.Text;
            Double i;
            if (!textBox3.Text.Equals(""))
            {
                if (!Double.TryParse(text, out i))
                {
                    MessageBox.Show("Сумма должна состоять только из цифр. Разрешен только разделяющий знак ','!", "Неверная сумма", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textBox3.Text = text.Substring(0, text.Length - 1);  //удаляет последний символ (который некорректный)
                    return;
                }
            }
            
        }

        private void showClients()
        {
            DataSet ds1 = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter("select name as 'Название компании-заказчика', id  from u0250751_enotik.Clients where name like '" + textBox2.Text + "%'", dataBaseConnection);
            da.Fill(ds1, "Клиенты");
            dataGridView2.DataSource = ds1.Tables["Клиенты"];
            dataGridView2.Columns[1].Visible = false;
            dataGridView2.Update();
        }
        private void RefreshData()      //обновление данных на форме
        {
            ds = new DataSet();
            try
            {
                String search = textBox1.Text;
                da = new SqlDataAdapter("Select name as 'Название проекта', (select name from u0250751_enotik.Clients where id=id_client) as 'Заказчик', id, summa from u0250751_enotik.Projects where name like '" + search + "%' order by name", dataBaseConnection);
                da.Fill(ds, "Проекты");
                dataGridView1.DataSource = ds.Tables["Проекты"];
                dataGridView1.Columns[2].Visible = false;
                dataGridView1.Columns[3].Visible = false;
                dataGridView1.Update();
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            showSum();
        }

        private void showSum()
        {
            Decimal summa = (Decimal)dataGridView1["summa", dataGridView1.CurrentCell.RowIndex].Value;
            Double sum = Double.Parse(summa.ToString());
            label9.Text = sum.ToString();
            label9.Visible = true;
            label12.Visible = true;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            showSum();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            showClients();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            String nazv = name.Text;     
            String summa = textBox3.Text;
            int client = (int)dataGridView2["id", dataGridView2.CurrentCell.RowIndex].Value;
            if (nazv.Equals(""))
            {
                MessageBox.Show("Введите название нового проекта.", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (client.Equals(""))
            {
                MessageBox.Show("Выберите заказчика", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                SqlCommand cmd = new SqlCommand("Insert into u0250751_enotik.Projects values('" + client + "', '" + nazv + "', " + summa + ")", dataBaseConnection);
                SqlCommand cmd1 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + login + "', 'Открыл проект " + nazv + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                dataBaseConnection.Open();
                cmd.ExecuteNonQuery();
                cmd1.ExecuteNonQuery();
                dataBaseConnection.Close();
                regProject.Visible = false;
                textBox3.Clear();
                textBox2.Clear();
                RefreshData();
                this.Width = 505;
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }
    }
}
