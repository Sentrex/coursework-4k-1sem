﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace kursach
{
    public partial class MainForm : Form
    {
        SqlConnection dataBaseConnection = Resources.getConnection();
        String userName;
        int rights;
        String name;
        public MainForm(int rights, String userName, String name)
        {
            this.rights = rights;
            this.userName = userName;
            this.name = name;
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            label6.Text = name;
            switch (rights)
            {
                case 1:
                    label4.Text = "Гость системы";
                    break;
                case 2:
                    label4.Text = "Сотрудник отдела управления проектами";
                    break;
                case 3:
                    label4.Text = "Сотрудник отдела кадров";
                    break;
                case 4:
                    label4.Text = "Сотрудник отдела работы с клиентами";
                    break;
                case 5:
                    label4.Text = "Администратор";
                    admPanel.Visible = true;
                    break;
                case 6:
                    label4.Text = "Директор";
                    admPanel.Visible = true;
                    break;
            }
            this.ControlBox = false;
        }

        #region Меню
        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SqlCommand cmd1 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + userName + "', 'Выход из системы', '" + DateTime.Now + "', '" + DateTime.Today + "')", dataBaseConnection);
            try
            {
                dataBaseConnection.Open();
                cmd1.ExecuteNonQuery();
                dataBaseConnection.Close();
                Application.Exit();
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
            
        }
        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Курсовая работа Шибалова Романа Сергеевича", "О программе", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void admPanel_Click(object sender, EventArgs e)
        {
            if (rights < 5)
            {
                MessageBox.Show("Доступ запрещен.", "Не хватает прав", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else
            {
                AdminPanel adm = new AdminPanel(userName, rights);
                adm.ShowDialog();
            }
        }

        private void контрольПерсоналаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((rights == 3) || (rights == 5) || (rights == 6))
            {
                Employees empl = new Employees(userName);
                Resources.setMainForm(this);
                empl.ShowDialog();
            }
            else
            {
                MessageBox.Show("Доступ запрещен.\nЛибо не хватает прав доступа, либо Вы не являетесь сотрудником данного отдела.", "Не хватает прав", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        private void контрольДолжностейToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((rights == 3) || (rights == 5) || (rights == 6))
            {
                Positions pos = new Positions(userName);
                pos.ShowDialog();
            }
            else
            {
                MessageBox.Show("Доступ запрещен.\nЛибо не хватает прав доступа, либо Вы не являетесь сотрудником данного отдела.", "Не хватает прав", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        private void отделРаботыСКлиентамиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((rights == 4) || (rights == 5) || (rights == 6))
            {
                Clients cl = new Clients(userName);
                cl.ShowDialog();
            }
            else
            {
                MessageBox.Show("Доступ запрещен.\nЛибо не хватает прав доступа, либо Вы не являетесь сотрудником данного отдела.", "Не хватает прав", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        private void логИзмененийБазыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (rights == 5 || rights == 6)
            {
                Log log = new Log(rights);
                log.Show();
            }
            else
            {
                MessageBox.Show("Вам недоступен данный отчет.", "Нехватка прав доступа",  MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        private void сменитьПользователяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SqlCommand cmd1 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + userName + "', 'Выход из системы', '" + DateTime.Now + "', '" + DateTime.Today + "')", dataBaseConnection);
            try
            {
                dataBaseConnection.Open();
                cmd1.ExecuteNonQuery();
                dataBaseConnection.Close();
                this.Close();
                Resources.changeUser();
                Resources.getAuth().Show();
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void выходИзПрограммыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SqlCommand cmd1 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + userName + "', 'Выход из системы', '" + DateTime.Now + "', '" + DateTime.Today + "')", dataBaseConnection);
            try
            {
                dataBaseConnection.Open();
                cmd1.ExecuteNonQuery();
                dataBaseConnection.Close();
                Application.Exit();
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void управлениеНовымиПроектамиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((rights == 2) || (rights == 5) || (rights == 6))
            {
                Projects pr = new Projects(userName);
                pr.ShowDialog();
            }
            else
            {
                MessageBox.Show("Доступ запрещен.\nЛибо не хватает прав доступа, либо Вы не являетесь сотрудником данного отдела.", "Не хватает прав", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        private void управлениеСущствующимиПроектамиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((rights == 2) || (rights == 5) || (rights == 6))
            {
                ProjectToEmpl pte = new ProjectToEmpl(userName);
                pte.ShowDialog();
            }
            else
            {
                MessageBox.Show("Доступ запрещен.\nЛибо не хватает прав доступа, либо Вы не являетесь сотрудником данного отдела.", "Не хватает прав", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        private void архивЗавершенныхПроектовToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((rights == 2) || (rights == 5) || (rights == 6))
            {
                Archive arch = new Archive(userName);
                arch.ShowDialog();
            }
            else
            {
                MessageBox.Show("Доступ запрещен.\nЛибо не хватает прав доступа, либо Вы не являетесь сотрудником данного отдела.", "Не хватает прав", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        private void графикиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (rights == 6)
            {
                Graphics gr = new Graphics();
                gr.ShowDialog();
            }
            else
            {
                MessageBox.Show("Вам недоступен данный отчет.", "Нехватка прав доступа", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void ожидаемыеПоступленияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (rights == 6)
            {
                Moneys moneys = new Moneys();
                moneys.ShowDialog();
            }
            else
            {
                MessageBox.Show("Вам недоступен данный отчет.", "Нехватка прав доступа", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void статистикаПоПроектамToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (rights == 5 || rights == 6)
            {
                ReportProjects rp = new ReportProjects();
                rp.ShowDialog();
            }
            else
            {
                MessageBox.Show("Вам недоступен данный отчет.", "Нехватка прав доступа", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
    }
}
