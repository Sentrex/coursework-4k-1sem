﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace kursach
{
    public partial class Archive : Form
    {
        SqlConnection dataBaseConnection = Resources.getConnection();
        DataSet ds;
        SqlDataAdapter da;
        String login;
        DateTime time = DateTime.Today;
        public Archive(String user)
        {
            login = user;
            InitializeComponent();
        }

        private void RefreshData()
        {
            ds = new DataSet();
            try
            {
                String name = textBox1.Text;
                SqlDataAdapter da1;
                da1 = new SqlDataAdapter("select  name as 'Название проекта', id from u0250751_enotik.Projects where id in (select id_project from u0250751_enotik.Nazn_Projects where complete = 0) and name like '" + name + "%'", dataBaseConnection);
                da1.Fill(ds, "Проекты");
                dataGridView1.DataSource = ds.Tables["Проекты"];
                dataGridView1.Columns[1].Visible = false;
                dataGridView1.Update();
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void Archive_Load(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int proj = (int)dataGridView1["id", dataGridView1.CurrentCell.RowIndex].Value;
            try
            {
                if (MessageBox.Show("Завершая проект, вы подтверждаете, что получили оплату от заказчика.", "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    SqlCommand cmd = new SqlCommand("Update u0250751_enotik.Nazn_Projects set complete=1 where id_project = " + proj, dataBaseConnection);
                    SqlCommand cmd1 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + login + "', 'Завершил проект <<" + (String)dataGridView1["Название проекта", dataGridView1.CurrentCell.RowIndex].Value + ">> ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                    dataBaseConnection.Open();
                    cmd.ExecuteNonQuery();
                    cmd1.ExecuteNonQuery();
                    dataBaseConnection.Close();
                    MessageBox.Show("Проект успешно завершен.", "Успешное завершение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    RefreshData();
                }
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }
    }
}
