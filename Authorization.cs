﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace kursach
{
    public partial class Authorization : Form
    {
        SqlConnection dataBaseConnection = Resources.getConnection();
        int RightsLevel = 0;
        public Authorization()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Entering();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            clearRegF();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Registration();
        }        

        private void Authorization_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        #region pressEnter
        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {
            pressEnter(e, "login", textBox2);
        }

        private void pressEnter(KeyEventArgs e, String mode, TextBox obj)
        {
            if (e.KeyCode == Keys.Enter && !obj.Text.Equals(""))
            {
                if (mode.Equals("login"))
                {
                    Entering();
                    return;
                }
                else
                {
                    Registration();
                    return;
                }
            }
            if (e.KeyCode == Keys.Escape)
            {
                textBox1.Clear();
                textBox2.Clear();
                textBox1.Focus();
                clearRegF();
            }
            return;
        }

        
        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            pressEnter(e, "login", textBox1);
        }

        private void textBox6_KeyUp(object sender, KeyEventArgs e)
        {
            pressEnter(e, "reg", textBox6);
        }

        private void textBox5_KeyUp(object sender, KeyEventArgs e)
        {
            pressEnter(e, "reg", textBox5);
        }
        #endregion

        private void Registration()
        {
            String login = textBox3.Text;
            if (login == "")
            {
                MessageBox.Show("Задайте логин.", "Логин не введен", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            String pass = textBox4.Text;
            if (pass == "")
            {
                MessageBox.Show("Задайте пароль.", "Пароль не введен", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            String Name = textBox5.Text;
            if (Name == "")
            {
                MessageBox.Show("Представьтесь системе.", "Имя не введено", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            int fl = setRights();
            if (fl == -1)
                return;
            else
            {
                SqlCommand cmd = new SqlCommand();  //запрос будет тут
                cmd.Connection = dataBaseConnection;  //указываем к какой базе подключаться
                cmd.CommandText = @"Insert into u0250751_enotik.Users values ('" + login + "', '" + pass + "', '" + Name + "', " + RightsLevel + ", 0)";  //создаем текст запроса
                dataBaseConnection.Open();
                try
                {
                    cmd.ExecuteNonQuery();
                    DateTime time = DateTime.Today;
                    SqlCommand cmd1 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + login + "', 'Зарегистрировался', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                    cmd1.ExecuteNonQuery();
                }
                catch (SqlException)
                {
                    MessageBox.Show("Такой логин уже существует. Введите другой.", "Повторяющийся логин", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    textBox3.Focus();
                    textBox3.Clear();
                    return;
                }
                finally
                {
                    dataBaseConnection.Close();
                }
                groupBox1.Visible = false;
                textBox1.Focus();
                textBox3.Clear();
                textBox4.Clear();
                textBox5.Clear();
                MessageBox.Show("Регистрация успешно завершена! Теперь Вы можете войти под своей новой учетной записью!", "Успешная регистрация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                button2.Focus();
            }
        }

        private int setRights()
        {
            String rightpass = textBox6.Text;       //получили пароль регистрации
            DataSet dsr = new DataSet();
            SqlDataAdapter dar = new SqlDataAdapter("select код, пароль from u0250751_enotik.Rights where пароль = '" + rightpass + "'", dataBaseConnection);
            dar.Fill(dsr, "Пароли");
            DataTable dt = dsr.Tables["Пароли"];
            int count = dt.Rows.Count;
            if (count != 0)
            {
                DataRow dr = dt.Rows[0];
                RightsLevel = (int)dr["код"];
            }
            else
            {
                MessageBox.Show("Пароль регистрации введен неверно. Повторите ввод или обратитесь к администратору за актулаьным паролем.", "Пароль регистрации неверен", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox6.Clear();
                textBox6.Focus();
                return -1;
            }
            return 0;
        }

        private void Entering()
        {
            String login = textBox1.Text;
            if (login == "")
            {
                MessageBox.Show("Введите логин.", "Логин не введен", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox1.Focus();
                textBox2.Clear();
                return;
            }
            String pass = textBox2.Text;
            if (pass == "")
            {
                MessageBox.Show("Введите пароль.", "Пароль не введен", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox2.Focus();
                return;
            }
            SqlCommand cmd = new SqlCommand();      //новый запрос sql
            DataSet ds = new DataSet();             //новый набор данных
            try
            {
                SqlDataAdapter da = new SqlDataAdapter();
                cmd.CommandText = @"Select * from u0250751_enotik.Users where userName ='" + login + "' and pass = '" + pass + "'";   //сам запрос
                cmd.Connection = dataBaseConnection;    //коннект для базы
                da.SelectCommand = cmd;     //выполнение запроса
                da.Fill(ds, "Пользователь");    //заполнение
                int count = ds.Tables["Пользователь"].Rows.Count;
                if (count == 1)
                {
                    this.Enabled = false;
                    String Name = "";        //создаем переменную для имени
                    DataTable dt = new DataTable();  //создаем переменную под таблицу
                    dt = ds.Tables["Пользователь"];  // получаем таблицу
                    DataRow dr = dt.Rows[0];        //берем из таблицы первую (она же и единственная) строку
                    bool isBlocked = (bool)dr["isBlocked"];
                    if (isBlocked)
                    {
                        MessageBox.Show("Аккаунт заблокирован. Обратитесь к администратору за дальнейшими инструкциями", "Вы заблокированы.", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        this.Enabled = true;
                        ClearAuthForm();
                        return;
                    }
                    Name = (String)dr["Name"];      //получаем оттуда имя пользователя
                    String userName = (String)dr["userName"];
                    MessageBox.Show("Здравствуйте, " + Name + "!", "Приветствие", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DateTime time = DateTime.Today;
                    SqlCommand cmd1 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + login + "', 'Авторизация', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                    dataBaseConnection.Open();
                    cmd1.ExecuteNonQuery();
                    dataBaseConnection.Close();
                    RightsLevel = (int)dr["Rights"];
                    this.Visible = false;
                    Resources.setAuth(this);
                    MainForm MainForm = new MainForm(RightsLevel, userName, Name);
                    MainForm.Show();
                }
                else
                {
                    MessageBox.Show("Неверный логин или пароль. Повторите ввод.", "Ошибка авторизации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ClearAuthForm();
                    textBox1.Focus();
                }
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void clearRegF()
        {
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
        }

        private void ClearAuthForm()
        {
            textBox1.Clear();
            textBox2.Clear();
        }

        private void Authorization_Leave(object sender, EventArgs e)
        {
            ClearAuthForm();
        }

        private void Authorization_Activated(object sender, EventArgs e)
        {
            ClearAuthForm();
        }
    }
}
