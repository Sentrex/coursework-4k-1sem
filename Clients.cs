﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace kursach
{
    public partial class Clients : Form
    {
        SqlConnection dataBaseConnection = Resources.getConnection();
        DataSet ds;
        SqlDataAdapter da;
        String login;
        DateTime time = DateTime.Today;
        public Clients(String user)
        {
            login = user;
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Height = 541;
            regClient.Visible = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            regClient.Visible = false;
            this.Height = 435;
        }

        private void RefreshData()      //обновление данных на форме
        {
            ds = new DataSet();
            try
            {
                String search = textBox1.Text;
                da = new SqlDataAdapter("Select name as 'Название компании клиента', id from u0250751_enotik.Clients where name like '" + search + "%' order by name", dataBaseConnection);
                da.Fill(ds, "Клиенты");
                dataGridView1.DataSource = ds.Tables["Клиенты"];
                dataGridView1.Columns[1].Visible = false;
                dataGridView1.Update();
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void Clients_Load(object sender, EventArgs e)
        {
            RefreshData();
            this.Height = 443;
            this.ControlBox = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            String name = FIO.Text;
            if (FIO.Text.Equals(""))
            {
                MessageBox.Show("Введите название компании клиента", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                SqlCommand cmd = new SqlCommand("Insert into u0250751_enotik.Clients values('" + name + "')");
                SqlCommand cmd1 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + login + "', 'Зарегистриовал нового клиента " + name + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                cmd.Connection = dataBaseConnection;
                dataBaseConnection.Open();
                cmd.ExecuteNonQuery();
                cmd1.ExecuteNonQuery();
                dataBaseConnection.Close();
                regClient.Visible = false;
                FIO.Clear();
                RefreshData();
                this.Height = 443;
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }
    }
}
