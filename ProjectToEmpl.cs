﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace kursach
{
    public partial class ProjectToEmpl : Form
    {
        SqlConnection dataBaseConnection = Resources.getConnection();
        DataSet ds;
        SqlDataAdapter da;
        String login;
        DateTime time = DateTime.Today;
        public ProjectToEmpl(String user)
        {
            login = user;
            InitializeComponent();
        }

        private void ProjectToEmpl_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            RefreshDataProjects();
            RefreshDataEmployee();
            GetNameProj();
            GetFIO();
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RefreshDataProjects()      //обновление данных на форме
        {
            ds = new DataSet();
            try
            {
                String name = textBox1.Text;
                SqlDataAdapter da1;
                da1 = new SqlDataAdapter("select  name as 'Название проекта', id from u0250751_enotik.Projects where id not in (select id_project from u0250751_enotik.Nazn_Projects) and name like '" + name + "%'", dataBaseConnection);
                da1.Fill(ds, "Проекты");
                dataGridView1.DataSource = ds.Tables["Проекты"];
                dataGridView1.Columns[1].Visible = false;
                dataGridView1.Update();
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void RefreshDataEmployee()      //обновление данных на форме
        {
            ds = new DataSet();
            try
            {
                String search = textBox2.Text;
                da = new SqlDataAdapter("Select name as ФИО, (select nazv from u0250751_enotik.Dolzn where id = id_dolzn) as Должность, id from u0250751_enotik.Employee where name like '" + search + "%' and id_dolzn in (select id from u0250751_enotik.Dolzn where nazv not like 'стажер') order by name", dataBaseConnection);
                da.Fill(ds, "Штат");
                dataGridView2.DataSource = ds.Tables["Штат"];
                dataGridView2.Columns[2].Visible = false;
                dataGridView2.Update();
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void GetNameProj()
        {
            String name = (String)dataGridView1["Название проекта", dataGridView1.CurrentCell.RowIndex].Value;
            label6.Text = name;
        }

        private void GetFIO()
        {
            String name = (String)dataGridView2["ФИО", dataGridView2.CurrentCell.RowIndex].Value;
            label8.Text = name;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            RefreshDataProjects();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            RefreshDataEmployee();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            GetNameProj();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            GetNameProj();
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            GetFIO();
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            GetFIO();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int empl = (int)dataGridView2["id", dataGridView2.CurrentCell.RowIndex].Value;
            int proj = (int)dataGridView1["id", dataGridView1.CurrentCell.RowIndex].Value;
            DateTime ending = dateTimePicker1.Value;
            if (ending < DateTime.Today)
                    {
                        MessageBox.Show("Дата завершения проекта не может быть раньше текущей!", "Неверная дата", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
            try
            {
                if (MessageBox.Show("Подтвердите данные:\n\n" +
                            "Название проекта:             " + (String)dataGridView1["Название проекта", dataGridView1.CurrentCell.RowIndex].Value + "\n" +
                            "ФИО ответственного:        " + (String)dataGridView2["ФИО", dataGridView2.CurrentCell.RowIndex].Value + "\n" +
                            "Дата окончания проекта: " + ending.ToString("dd.MM.yyyy"), "Подтвердите введенные данные", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.OK)
                {
                    SqlCommand cmd = new SqlCommand("Insert into u0250751_enotik.Nazn_Projects values (" + proj + ", " + empl + ", '" + ending.ToString("yyyy-MM-dd") + "', 0)", dataBaseConnection);
                    SqlCommand cmd2 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + login + "', 'Назначил проект <<" + label6.Text + ">> сотруднику " + label8.Text + "', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                    dataBaseConnection.Open();
                    cmd.ExecuteNonQuery();
                    cmd2.ExecuteNonQuery();
                    dataBaseConnection.Close();
                    MessageBox.Show("Назначение проекта прошло успешно.", "Успешное выполнение операции", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    RefreshDataEmployee();
                    RefreshDataProjects();
                }
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }
    }
}
