﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;

namespace kursach
{
    public partial class Graphics : Form
    {
        SqlConnection dataBaseConnection = Resources.getConnection();
        DataSet ds;
        public Graphics()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Graphics_Load(object sender, EventArgs e)
        {
            ds = new DataSet();
            try
            {
                chart1.Series.Clear();
                SqlDataAdapter da1;
                da1 = new SqlDataAdapter("select  t2.name as 'Название компании клиента', count(*) as 'количество проектов' from u0250751_enotik.Projects t1, u0250751_enotik.Clients t2 where id_client= t2.id group by t2.name", dataBaseConnection);
                da1.Fill(ds, "Отчет");
                Hashtable ht = new Hashtable();
                for (int i = 0; i < ds.Tables["Отчет"].Rows.Count; i++)
                {
                    ht.Add(ds.Tables["Отчет"].Rows[i][0].ToString(), ds.Tables["Отчет"].Rows[i][1].ToString());
                }
                ICollection keys = ht.Keys;
                int k=2;
                foreach (string s in keys)
                {
                    chart1.Series.Add(s);
                    chart1.Series[s].Points.AddXY(k, ht[s]);
                    k++;
                }
                chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }
    }
}
