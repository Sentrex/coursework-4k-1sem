﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace kursach
{
    public partial class ReportProjects : Form
    {
        SqlConnection dataBaseConnection = Resources.getConnection();
        DataSet ds;
        public ReportProjects()
        {
            InitializeComponent();
        }

        private void ReportProjects_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            if (radioButton1.Checked)
            {
                RefreshData(1);
            }
            else
            {
                RefreshData(2);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RefreshData(int mode)
        {
            ds = new DataSet();
            String sqlCurr = "select  name as 'Название проекта', id from u0250751_enotik.Projects where id in (select id_project from u0250751_enotik.Nazn_Projects where complete=0)";
            String sqlEnd = "select  name as 'Название проекта', id from u0250751_enotik.Projects where id in (select id_project from u0250751_enotik.Nazn_Projects where complete=1)";
            try
            {
                String cmd1;
                if (mode == 1)
                {
                    cmd1 = sqlCurr;
                }
                else
                {
                    cmd1 = sqlEnd;
                }
                SqlDataAdapter da1;
                da1 = new SqlDataAdapter(cmd1, dataBaseConnection);
                da1.Fill(ds, "Проекты");
                dataGridView1.DataSource = ds.Tables["Проекты"];
                dataGridView1.Columns[1].Visible = false;
                dataGridView1.Update();
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                RefreshData(1);
            }
            else
            {
                RefreshData(2);
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                RefreshData(2);
            }
            else
            {
                RefreshData(1);
            }
        }
    }
}
