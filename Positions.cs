﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace kursach
{
    public partial class Positions : Form
    {
        SqlConnection dataBaseConnection = Resources.getConnection();
        DataSet ds;
        SqlDataAdapter da;
        String login;
        DateTime time = DateTime.Today;
        public Positions(String user)
        {
            login = user;
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Positions_Load(object sender, EventArgs e)
        {
            this.Height = 445;
            this.Width = 687;
            RefreshData();
            this.ControlBox = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RegPosition.Visible = true;
            this.Height = 543;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            RegPosition.Visible = false;
            this.Height = 445;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            String name = FIO.Text;      
            if (FIO.Text.Equals(""))
            {
                MessageBox.Show("Введите ФИО нового сотрудника", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                if (name.Equals("директор"))
                {
                    MessageBox.Show("Добавление должности директора в базу запрещено.", "Попытка регитрации отклонена", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    SqlCommand cmd3 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + login + "', 'Попытка регистрации должности директора! ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                    dataBaseConnection.Open();
                    cmd3.ExecuteNonQuery();
                    dataBaseConnection.Close();
                    return;
                }
                SqlCommand cmd = new SqlCommand("Insert into u0250751_enotik.Dolzn values('" + name + "')");
                SqlCommand cmd1 = new SqlCommand("Insert into u0250751_enotik.LogChanges values ('" + login + "', 'Открыл должность " + name + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                cmd.Connection = dataBaseConnection;
                dataBaseConnection.Open();
                cmd.ExecuteNonQuery();
                cmd1.ExecuteNonQuery();
                dataBaseConnection.Close();
                RegPosition.Visible = false;
                FIO.Clear();
                RefreshData();
                this.Height = 443;
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void RefreshData()      //обновление данных на форме
        {
            ds = new DataSet();
            try
            {
                String search = textBox1.Text;
                da = new SqlDataAdapter("Select nazv as 'Должность', id from u0250751_enotik.Dolzn where nazv like '" + search + "%' order by nazv", dataBaseConnection);
                da.Fill(ds, "Должности");
                dataGridView1.DataSource = ds.Tables["Должности"];
                dataGridView1.Columns[1].Visible = false;
                dataGridView1.Update();
            }
            catch (SqlException)
            {
                Resources.baseUnavailable();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            RefreshData();
        }
    }
}
